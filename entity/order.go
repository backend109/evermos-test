package entity

import "database/sql"

// * Order represents order table
type Order struct {
	ID          int64         `json:"id"`
	ProductID   int64         `json:"product_id"`
	ProductName string        `json:"product_name"`
	Quantity    int32         `json:"quantity"`
	CreatedAt   int64         `json:"created_at"`
	UpdatedAt   sql.NullInt64 `json:"updated_at"`
	DeletedAt   sql.NullInt64 `json:"deleted_at"`
}

// ================================================ //
// * params and rows
// ================================================ //

type CreateOrderParams struct {
	ProductID   int64  `json:"product_id"`
	ProductName string `json:"product_name"`
	Quantity    int32  `json:"quantity"`
	CreatedAt   int64  `json:"created_at"`
}
