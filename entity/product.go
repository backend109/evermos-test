package entity

import "database/sql"

// * Product represents product table
type Product struct {
	ID        int64         `json:"id"`
	Name      string        `json:"name"`
	Stock     int32         `json:"stock"`
	CreatedAt int64         `json:"created_at"`
	UpdatedAt sql.NullInt64 `json:"updated_at"`
	DeletedAt sql.NullInt64 `json:"deleted_at"`
}

// ================================================ //
// * params and rows
// ================================================ //

type GetProductsRow struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	Stock int32  `json:"stock"`
}

type GetProductByIDForUpdateRow struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	Stock int32  `json:"stock"`
}

type UpdateStockWithIDParams struct {
	Stock     int32         `json:"stock"`
	UpdatedAt sql.NullInt64 `json:"updated_at"`
	ID        int64         `json:"id"`
}
