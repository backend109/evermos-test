# Evermos Backend Test

## Clone Repository

```git
git clone https://gitlab.com/backend109/evermos-test
```

## Online Store

### Why it happened

During the 12.12 event, so many customers want to get a specific product, and the customer makes the order simultaneously.

* The backend systems doesn't prevent when customers make orders at the same time
* The Backend systems doesn't lock the product stock when customers make an order for a specific product
* Example, the stock of product is 2, and at the same time, the system receives 3 customers who want to make an order with the same product
* Result, the stock of product will be minus because 3 customer order same product at the same time

### Solution

Solution Purpose for this case

* The backend systems must lock the product stock when customers make an order
* When many customers make orders at the same time for a specific product, just one customer can update the stock, and the other customer will be waiting since the previous customer success create the order

### How its work

How the solution works

* Customer request to create order
* Backend system will lock the stock of specific product with the transaction of database ( ex: select for update )
* Backend system will do some process of order
* When the order is successfully created, backend systems will release the transaction

### How to Run

#### Run Locally

* install some dependencies ( Go 1.16+ )
  
```
make install
```

Go 1.15 and below
  
```
make install-legacy
```


* create new config file

```
cp main.example.json main.json
```

* change db config on main.json and Makefile
* run the migration

```
make migrate-up
```

* run the project

```
make local
```

* run the test ( must run on new terminal )

```
make test-code
```

#### Run With Docker

When you run with Docker, the database will use the rds db

* run the project

```
make compose-up-local
```

#### API Documentation

* when you run project locally you can check documentation on <http://localhost:8199/swagger/index.html>
* or you can check documentation on <https://api-evermos.dhfnr.com/swagger/index.html>


## Treasure Hunt

### How to Run

``` go
go run treasure_hunt/main.go
```