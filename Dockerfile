FROM golang:1.16.6-alpine3.13 as builder

WORKDIR /app

COPY . ./

WORKDIR /
RUN go get -u github.com/swaggo/swag/cmd/swag

WORKDIR /app 
RUN swag init

RUN go mod download

RUN GOOS=linux GOARCH=amd64 go build -o main .

## Distribution Production
FROM alpine:3.13

RUN apk add --no-cache --upgrade ca-certificates tzdata 

WORKDIR /app

COPY --from=builder /app/main ./
COPY --from=builder /app/config/app/production.json ./config/app/

CMD [ "/app/main" ]