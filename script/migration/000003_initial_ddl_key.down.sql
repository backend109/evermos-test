BEGIN;

ALTER TABLE "orders" DROP CONSTRAINT IF EXISTS "product_id_orders_id_products_fkey";

COMMIT;