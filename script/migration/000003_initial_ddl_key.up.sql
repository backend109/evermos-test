BEGIN;

ALTER TABLE "orders" ADD CONSTRAINT "product_id_orders_id_products_fkey" FOREIGN KEY ("product_id") REFERENCES "products" ("id");

COMMIT;