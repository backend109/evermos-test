BEGIN;

DROP TABLE IF EXISTS "products" CASCADE;
DROP TABLE IF EXISTS "orders" CASCADE;

CREATE TABLE IF NOT EXISTS "products" (
    "id" bigserial NOT NULL PRIMARY KEY,

    "name" text NOT NULL,
    "stock" int NOT NULL,

    "created_at" bigint NOT NULL,
    "updated_at" bigint,
    "deleted_at" bigint
);

CREATE TABLE IF NOT EXISTS "orders" (
    "id" bigserial NOT NULL PRIMARY KEY,

    "product_id" bigint NOT NULL,
    "product_name" text NOT NULL,
    "quantity" int NOT NULL,

    "created_at" bigint NOT NULL,
    "updated_at" bigint,
    "deleted_at" bigint
);

COMMIT;