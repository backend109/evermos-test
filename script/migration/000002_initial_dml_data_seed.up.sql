BEGIN;

INSERT INTO "products" ("name","stock", "created_at") VALUES
    ('Water', 10 , extract(epoch from now())),
    ('Beef', 5 , extract(epoch from now())),
    ('Kiwi', 3 , extract(epoch from now())),
    ('Gamis', 7 , extract(epoch from now())),
    ('Hijab', 2 , extract(epoch from now()));

COMMIT;