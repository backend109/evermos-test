package test

import (
	"encoding/json"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/backend109/evermos-test/schema/request"
)

const (
	address = "http://localhost:8119/v1/orders"
)

func TestConcurrencyOrder(t *testing.T) {
	req, _ := json.Marshal(&request.CreateOrderRequest{
		ProductID: 5,
		Quantity:  1,
	})

	req2, _ := json.Marshal(&request.CreateOrderRequest{
		ProductID: 5,
		Quantity:  2,
	})

	body := strings.NewReader(string(req))
	body2 := strings.NewReader(string(req2))

	responseOne := make(chan *http.Response)
	responseTwo := make(chan *http.Response)

	go func(t *testing.T) {
		res1, err := http.Post(address, "application/json", body)
		assert.Nil(t, err)
		responseOne <- res1
	}(t)

	go func(t *testing.T) {
		res2, err := http.Post(address, "application/json", body2)
		assert.Nil(t, err)
		responseTwo <- res2
	}(t)

	response1 := <-responseOne
	response2 := <-responseTwo

	if response1.StatusCode == http.StatusOK {
		assert.NotEqual(t, response2.StatusCode, http.StatusOK)
	}

	if response2.StatusCode == http.StatusOK {
		assert.NotEqual(t, response1.StatusCode, http.StatusOK)
	}
}
