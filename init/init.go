package init

// StartAppInit init all server needs
func StartAppInit() (config *Config) {
	setupLogger()

	config = setupMainConfig()

	return
}
