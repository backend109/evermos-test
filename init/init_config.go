package init

import (
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/backend109/evermos-test/pkg/util"
	log "go.uber.org/zap"
)

type Config struct {
	API struct {
		Port string `mapstructure:"port"`
	} `mapstructure:"api"`
	Context struct {
		Timeout int `mapstructure:"timeout"`
	} `mapstructure:"context"`
	Database struct {
		Pg struct {
			Host                  string `mapstructure:"host"`
			Port                  string `mapstructure:"port"`
			Dbname                string `mapstructure:"dbname"`
			User                  string `mapstructure:"user"`
			Password              string `mapstructure:"password"`
			Sslmode               string `mapstructure:"sslmode"`
			MaxOpenConnection     int    `mapstructure:"max_open_connection"`
			MaxIdleConnection     int    `mapstructure:"max_idle_connection"`
			MaxConnectionLifetime string `mapstructure:"max_connection_lifetime"`
		} `mapstructure:"pg"`
	} `mapstructure:"database"`
}

// setupMainConfig loads app config (json) with viper
func setupMainConfig() (config *Config) {
	log.S().Info("Executing init/config")

	conf := false

	if util.IsProductionEnv() {
		conf = true
		log.S().Info("prod config")
		viper.SetConfigFile("config/app/production.json")
		err := viper.ReadInConfig()
		if err != nil {
			log.S().Info("err: ", err)
		}
	}

	if util.IsFileorDirExist("main.json") {
		conf = true
		log.S().Info("Local main.json file is found, now assigning it with default config")
		viper.SetConfigFile("main.json")
		err := viper.ReadInConfig()
		if err != nil {
			log.S().Info("err: ", err)
		}
	}

	if !conf {
		log.S().Fatal("Config is required")
	}

	viper.SetEnvPrefix(`app`)
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.AutomaticEnv()

	err := viper.Unmarshal(&config)
	if err != nil {
		log.S().Fatal("err: ", err)
	}

	log.S().Info("Config - APP_ENV: ", util.GetEnv())

	return
}
