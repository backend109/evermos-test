# DATABASE
DB_USER=postgres
DB_PASSWORD=postgres
DB_HOST=127.0.0.1
DB_PORT=5432
DB_NAME=evermos
DB_SSL=disable

migrate-up:
	migrate -source file:./script/migration/ -verbose -database postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSL} up

migrate-up-one:
	migrate -source file:./script/migration/ -verbose -database postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSL} up 1

migrate-down:
	migrate -source file:./script/migration/ -verbose -database postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSL} down 1

migrate-drop:
	migrate -source file:./script/migration/ -verbose -database postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSL} drop

# Local
PROJECT_NAME=evermos-test

install:
	cd .. && \
	go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest && \
	go get -u github.com/swaggo/swag/cmd/swag && go get -u github.com/cosmtrek/air && \
	cd ${PROJECT_NAME} && swag init

install-legacy:
	cd .. && \
	go get -tags 'postgres' -u github.com/golang-migrate/migrate/cmd/migrate && \
	go get -u github.com/swaggo/swag/cmd/swag && go get -u github.com/cosmtrek/air && \
	cd ${PROJECT_NAME} && swag init

local:
	air -c config/.air.toml

format:
	go fmt ./...

test-code:
	go test -v ./...


# Docker
compose-pull:
	docker-compose pull

compose-up:
	docker-compose up -d

compose-up-local:
	docker-compose -f docker-compose-local.yml up -d

docker-clear:
	docker container stop evermost-be-test && \
	docker container rm evermost-be-test && \
	docker image rm evermos-test_app

# Git Shortcut
rebase-master:
	git checkout master && git pull origin master && git checkout @{-1} && git rebase master

push:
	go fmt ./... && git push origin HEAD

push-force:
	go fmt ./... && git push -f origin HEAD
