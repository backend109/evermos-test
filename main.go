package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	_productHttpHandler "gitlab.com/backend109/evermos-test/module/product/handler/http"
	_productPgRepo "gitlab.com/backend109/evermos-test/module/product/repository/pg"
	_product "gitlab.com/backend109/evermos-test/module/product/usecase"

	_orderHttpHandler "gitlab.com/backend109/evermos-test/module/order/handler/http"
	_orderPgRepo "gitlab.com/backend109/evermos-test/module/order/repository/pg"
	_order "gitlab.com/backend109/evermos-test/module/order/usecase"

	_repoPgRepo "gitlab.com/backend109/evermos-test/module/repository/repository/pg"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	appMiddleware "gitlab.com/backend109/evermos-test/middleware"

	_ "gitlab.com/backend109/evermos-test/docs"

	echoSwagger "github.com/swaggo/echo-swagger"
	appInit "gitlab.com/backend109/evermos-test/init"
	log "go.uber.org/zap"
)

var cfg *appInit.Config

// * Start pre-requisite app dependencies
func init() {
	cfg = appInit.StartAppInit()
}

func main() {

	// * get PG Conn Instance
	pgDb, err := appInit.ConnectToPGServer()
	if err != nil {
		log.S().Fatal(err)
	}

	// * init router
	e := echo.New()

	// * Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(appMiddleware.EchoCORS())

	// * init additional part of project

	timeoutContext := time.Duration(cfg.Context.Timeout) * time.Second
	log := log.S()

	// * DI: Repository & Usecase
	productPgRepo := _productPgRepo.NewRepository(pgDb)
	orderPgRepo := _orderPgRepo.NewRepository(pgDb)
	repoAggregatorPgRepo := _repoPgRepo.NewRepository(pgDb)

	productUc := _product.NewProductUsecase(cfg, timeoutContext, log, productPgRepo)
	ordertUc := _order.NewOrderUsecase(cfg, timeoutContext, log, productPgRepo, orderPgRepo, repoAggregatorPgRepo)
	// * End of DI Steps

	// * Routes
	_productHttpHandler.NewProductHandler(e, productUc)
	_orderHttpHandler.NewOrderHandler(e, ordertUc)

	// * Route for check server healtz
	e.GET("/healtz", func(c echo.Context) error {
		return c.String(http.StatusOK, "Server is Healthy")
	})

	// * swagger init
	e.GET("/swagger/*", echoSwagger.WrapHandler)

	// * start the http server
	go runHTTPHandler(e, cfg)

	// * code for graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}

}

// * runHTTPHandler used to start the http server
func runHTTPHandler(e *echo.Echo, cfg *appInit.Config) {
	if err := e.Start(cfg.API.Port); err != nil {
		fmt.Println("shutting down the server")
	}
}
