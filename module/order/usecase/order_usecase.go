package usecase

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/backend109/evermos-test/constant"
	"gitlab.com/backend109/evermos-test/entity"
	appInit "gitlab.com/backend109/evermos-test/init"
	orderPg "gitlab.com/backend109/evermos-test/module/order/repository/pg"
	productPg "gitlab.com/backend109/evermos-test/module/product/repository/pg"
	aggPg "gitlab.com/backend109/evermos-test/module/repository/repository/pg"
	"gitlab.com/backend109/evermos-test/schema/request"
	"go.uber.org/zap"
)

type OrderUsecase struct {
	cfg            *appInit.Config
	contextTimeout time.Duration
	log            *zap.SugaredLogger
	productPgRepo  productPg.Repository
	orderPgRepo    orderPg.Repository
	aggPgRepo      aggPg.Repository
}

func NewOrderUsecase(cfg *appInit.Config, timeout time.Duration, log *zap.SugaredLogger, productPgRepo productPg.Repository, orderPgRepo orderPg.Repository,
	aggPgRepo aggPg.Repository) Usecase {
	return &OrderUsecase{
		cfg:            cfg,
		contextTimeout: timeout,
		log:            log,
		productPgRepo:  productPgRepo,
		orderPgRepo:    orderPgRepo,
		aggPgRepo:      aggPgRepo,
	}
}

// * CreateOrder used to create a new order ( business logic )
func (u *OrderUsecase) CreateOrder(ctx context.Context, req *request.CreateOrderRequest) (err error) {
	ctx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	//* start db transactions
	tx, err := u.aggPgRepo.BeginTx(ctx)
	if err != nil {
		u.log.Error(err)
		return
	}

	product, err := u.productPgRepo.WithTx(tx).GetProductByIDForUpdate(ctx, req.ProductID)
	if err != nil {
		u.log.Error(err)
		u.aggPgRepo.RollbackTx(tx)
		return
	}

	// * check product stock
	if (product.Stock - req.Quantity) < 0 {
		u.aggPgRepo.RollbackTx(tx)
		err = constant.ErrorCommonOutOfStock
		return
	}

	timeNowUnix := time.Now().Unix()
	// * update product stock
	updateProductStockParam := &entity.UpdateStockWithIDParams{
		ID:        req.ProductID,
		Stock:     product.Stock - req.Quantity,
		UpdatedAt: sql.NullInt64{Int64: timeNowUnix, Valid: true},
	}

	err = u.productPgRepo.WithTx(tx).UpdateStockWithID(ctx, updateProductStockParam)
	if err != nil {
		u.log.Error(err)
		u.aggPgRepo.RollbackTx(tx)
		return
	}

	// * create order data
	craeteOrderParams := &entity.CreateOrderParams{
		ProductID:   req.ProductID,
		ProductName: product.Name,
		Quantity:    req.Quantity,
		CreatedAt:   timeNowUnix,
	}

	err = u.orderPgRepo.WithTx(tx).CreateOrder(ctx, craeteOrderParams)
	if err != nil {
		u.log.Error(err)
		u.aggPgRepo.RollbackTx(tx)
		return
	}

	// * commit the transactions
	u.aggPgRepo.CommitTx(tx)

	return
}
