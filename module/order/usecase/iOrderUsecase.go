package usecase

import (
	"context"

	"gitlab.com/backend109/evermos-test/schema/request"
)

type Usecase interface {
	CreateOrder(ctx context.Context, req *request.CreateOrderRequest) (err error)
}
