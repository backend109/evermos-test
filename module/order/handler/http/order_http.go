package http

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/backend109/evermos-test/constant"
	"gitlab.com/backend109/evermos-test/module/order/usecase"
	"gitlab.com/backend109/evermos-test/pkg/util"
	"gitlab.com/backend109/evermos-test/schema/request"
)

type OrderHandler struct {
	orderUsecase usecase.Usecase
}

func NewOrderHandler(e *echo.Echo, us usecase.Usecase) {
	handler := &OrderHandler{
		orderUsecase: us,
	}

	routerV1 := e.Group("/v1/orders")
	routerV1.POST("", handler.CreateOrder)
}

// CreateOrder godoc
// @Summary Create a new order
// @Description Create a new order
// @Tags Order
// @Accept json
// @Produce json
// @Param request body request.CreateOrderRequest true "Request Body"
// @Success 201 {object} response.SwaggerCreateOrder
// @Failure 400 {object} response.Base
// @Failure 422 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/orders [post]
func (h *OrderHandler) CreateOrder(c echo.Context) error {
	req := request.CreateOrderRequest{}
	ctx := c.Request().Context()

	// parsing
	err := util.ParsingParameter(c, &req)
	if err != nil {
		return util.ErrorParsing(c, err, nil)
	}

	// validate
	err = util.ValidateParameter(c, &req)
	if err != nil {
		return util.ErrorValidate(c, err, nil)
	}

	err = h.orderUsecase.CreateOrder(ctx, &req)
	if err != nil {
		return util.ErrorResponse(c, err, nil)
	}

	return util.CreatedResponse(c, string(constant.ResponseMessageSuccessCreateOrder), nil)
}
