package pg

import (
	"context"
	"database/sql"

	"gitlab.com/backend109/evermos-test/entity"
)

type Repository interface {
	CreateOrder(ctx context.Context, arg *entity.CreateOrderParams) error

	// Tx
	WithTx(tx *sql.Tx) *Queries
}
