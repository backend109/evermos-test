package pg

import (
	"context"
	"database/sql"

	"gitlab.com/backend109/evermos-test/entity"
)

// SQLStore provides all functions to execute db queries and transactions.
type SQLStore struct {
	*Queries
	db *sql.DB
}

// NewRepository creates a new store
func NewRepository(db *sql.DB) Repository {
	return &SQLStore{
		db:      db,
		Queries: New(db),
	}
}

const createOrder = `-- name: CreateOrder :exec
INSERT INTO orders (
    product_id,
    product_name,
    quantity,
    created_at
) VALUES (
    $1,
    $2,
    $3,
    $4
)
`

func (q *Queries) CreateOrder(ctx context.Context, arg *entity.CreateOrderParams) error {
	_, err := q.exec(ctx, q.createOrderStmt, createOrder,
		arg.ProductID,
		arg.ProductName,
		arg.Quantity,
		arg.CreatedAt,
	)
	return err
}
