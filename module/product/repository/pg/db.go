package pg

import (
	"context"
	"database/sql"
	"fmt"
)

type DBTX interface {
	ExecContext(context.Context, string, ...interface{}) (sql.Result, error)
	PrepareContext(context.Context, string) (*sql.Stmt, error)
	QueryContext(context.Context, string, ...interface{}) (*sql.Rows, error)
	QueryRowContext(context.Context, string, ...interface{}) *sql.Row
}

func New(db DBTX) *Queries {
	return &Queries{db: db}
}

func Prepare(ctx context.Context, db DBTX) (*Queries, error) {
	q := Queries{db: db}
	var err error
	if q.getProductByIDForUpdateStmt, err = db.PrepareContext(ctx, getProductByIDForUpdate); err != nil {
		return nil, fmt.Errorf("error preparing query GetProductByIDForUpdate: %w", err)
	}
	if q.getProductsStmt, err = db.PrepareContext(ctx, getProducts); err != nil {
		return nil, fmt.Errorf("error preparing query GetProducts: %w", err)
	}
	if q.updateStockWithIDStmt, err = db.PrepareContext(ctx, updateStockWithID); err != nil {
		return nil, fmt.Errorf("error preparing query UpdateStockWithID: %w", err)
	}
	return &q, nil
}

func (q *Queries) Close() error {
	var err error
	if q.getProductByIDForUpdateStmt != nil {
		if cerr := q.getProductByIDForUpdateStmt.Close(); cerr != nil {
			err = fmt.Errorf("error closing getProductByIDForUpdateStmt: %w", cerr)
		}
	}
	if q.getProductsStmt != nil {
		if cerr := q.getProductsStmt.Close(); cerr != nil {
			err = fmt.Errorf("error closing getProductsStmt: %w", cerr)
		}
	}
	if q.updateStockWithIDStmt != nil {
		if cerr := q.updateStockWithIDStmt.Close(); cerr != nil {
			err = fmt.Errorf("error closing updateStockWithIDStmt: %w", cerr)
		}
	}
	return err
}

func (q *Queries) exec(ctx context.Context, stmt *sql.Stmt, query string, args ...interface{}) (sql.Result, error) {
	switch {
	case stmt != nil && q.tx != nil:
		return q.tx.StmtContext(ctx, stmt).ExecContext(ctx, args...)
	case stmt != nil:
		return stmt.ExecContext(ctx, args...)
	default:
		return q.db.ExecContext(ctx, query, args...)
	}
}

func (q *Queries) query(ctx context.Context, stmt *sql.Stmt, query string, args ...interface{}) (*sql.Rows, error) {
	switch {
	case stmt != nil && q.tx != nil:
		return q.tx.StmtContext(ctx, stmt).QueryContext(ctx, args...)
	case stmt != nil:
		return stmt.QueryContext(ctx, args...)
	default:
		return q.db.QueryContext(ctx, query, args...)
	}
}

func (q *Queries) queryRow(ctx context.Context, stmt *sql.Stmt, query string, args ...interface{}) *sql.Row {
	switch {
	case stmt != nil && q.tx != nil:
		return q.tx.StmtContext(ctx, stmt).QueryRowContext(ctx, args...)
	case stmt != nil:
		return stmt.QueryRowContext(ctx, args...)
	default:
		return q.db.QueryRowContext(ctx, query, args...)
	}
}

type Queries struct {
	db                          DBTX
	tx                          *sql.Tx
	getProductByIDForUpdateStmt *sql.Stmt
	getProductsStmt             *sql.Stmt
	updateStockWithIDStmt       *sql.Stmt
}

func (q *Queries) WithTx(tx *sql.Tx) *Queries {
	return &Queries{
		db:                          tx,
		tx:                          tx,
		getProductByIDForUpdateStmt: q.getProductByIDForUpdateStmt,
		getProductsStmt:             q.getProductsStmt,
		updateStockWithIDStmt:       q.updateStockWithIDStmt,
	}
}
