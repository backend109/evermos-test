package pg

import (
	"context"
	"database/sql"

	"gitlab.com/backend109/evermos-test/constant"
	"gitlab.com/backend109/evermos-test/entity"
)

// SQLStore provides all functions to execute db queries and transactions.
type SQLStore struct {
	*Queries
	db *sql.DB
}

// NewRepository creates a new store
func NewRepository(db *sql.DB) Repository {
	return &SQLStore{
		db:      db,
		Queries: New(db),
	}
}

const getProducts = `-- name: GetProducts :many
SELECT 
    id,
    name,
    stock
FROM products WHERE deleted_at IS NULL
`

func (q *Queries) GetProducts(ctx context.Context) (*[]entity.GetProductsRow, error) {
	rows, err := q.query(ctx, q.getProductsStmt, getProducts)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []entity.GetProductsRow{}
	for rows.Next() {
		var i entity.GetProductsRow
		if err := rows.Scan(&i.ID, &i.Name, &i.Stock); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return &items, nil
}

const getProductByIDForUpdate = `-- name: GetProductByIDForUpdate :one
SELECT 
    id,
    name,
    stock
FROM products WHERE id = $1 AND deleted_at IS NULL
FOR UPDATE
`

func (q *Queries) GetProductByIDForUpdate(ctx context.Context, pid int64) (*entity.GetProductByIDForUpdateRow, error) {
	row := q.queryRow(ctx, q.getProductByIDForUpdateStmt, getProductByIDForUpdate, pid)
	var i entity.GetProductByIDForUpdateRow
	err := row.Scan(&i.ID, &i.Name, &i.Stock)

	if err == sql.ErrNoRows {
		err = constant.ErrorPgProductNotFound
	}

	return &i, err
}

const updateStockWithID = `-- name: UpdateStockWithID :exec
UPDATE products SET 
    stock = $1,
    updated_at = $2
WHERE id = $3 AND deleted_at IS NULL
`

func (q *Queries) UpdateStockWithID(ctx context.Context, arg *entity.UpdateStockWithIDParams) error {
	_, err := q.exec(ctx, q.updateStockWithIDStmt, updateStockWithID, arg.Stock, arg.UpdatedAt, arg.ID)
	return err
}
