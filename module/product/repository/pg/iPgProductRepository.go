package pg

import (
	"context"
	"database/sql"

	"gitlab.com/backend109/evermos-test/entity"
)

type Repository interface {
	GetProducts(ctx context.Context) (*[]entity.GetProductsRow, error)
	GetProductByIDForUpdate(ctx context.Context, pid int64) (*entity.GetProductByIDForUpdateRow, error)
	UpdateStockWithID(ctx context.Context, arg *entity.UpdateStockWithIDParams) error

	// Tx
	WithTx(tx *sql.Tx) *Queries
}
