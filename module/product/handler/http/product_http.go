package http

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/backend109/evermos-test/constant"
	"gitlab.com/backend109/evermos-test/module/product/usecase"
	"gitlab.com/backend109/evermos-test/pkg/util"
)

type ProductHandler struct {
	productUsecase usecase.Usecase
}

func NewProductHandler(e *echo.Echo, us usecase.Usecase) {
	handler := &ProductHandler{
		productUsecase: us,
	}

	routerV1 := e.Group("/v1/products")
	routerV1.GET("", handler.GetListProduct)
}

// GetListProduct godoc
// @Summary Get List of Product
// @Description Get List of Product
// @Tags Product
// @Accept json
// @Produce json
// @Success 200 {object} response.SwaggerGetListProduct
// @Failure 400 {object} response.Base
// @Failure 401 {object} response.Base
// @Failure 422 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/products [get]
func (h *ProductHandler) GetListProduct(c echo.Context) error {
	ctx := c.Request().Context()

	res, err := h.productUsecase.GetProductList(ctx)
	if err != nil {
		return util.ErrorResponse(c, err, nil)
	}

	return util.SuccessResponse(c, string(constant.ResponseMessageSuccessGetProducts), res)
}
