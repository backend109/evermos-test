package usecase

import (
	"context"

	"gitlab.com/backend109/evermos-test/schema/response"
)

type Usecase interface {
	GetProductList(ctx context.Context) (res []response.GetProductResponse, err error)
}
