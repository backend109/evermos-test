package usecase

import (
	"context"
	"time"

	appInit "gitlab.com/backend109/evermos-test/init"
	productPg "gitlab.com/backend109/evermos-test/module/product/repository/pg"
	"gitlab.com/backend109/evermos-test/schema/response"
	"go.uber.org/zap"
)

type ProductUsecase struct {
	cfg            *appInit.Config
	contextTimeout time.Duration
	log            *zap.SugaredLogger
	productPgRepo  productPg.Repository
}

func NewProductUsecase(cfg *appInit.Config, timeout time.Duration, log *zap.SugaredLogger, productPgRepo productPg.Repository) Usecase {
	return &ProductUsecase{
		cfg:            cfg,
		contextTimeout: timeout,
		log:            log,
		productPgRepo:  productPgRepo,
	}
}

// * GetProductList used to get list of product data
func (u *ProductUsecase) GetProductList(ctx context.Context) (res []response.GetProductResponse, err error) {
	ctx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	products, err := u.productPgRepo.GetProducts(ctx)
	if err != nil {
		u.log.Errorf("error getting products : %v", err)
		return
	}

	// * mapping to response
	for _, product := range *products {
		res = append(res, response.GetProductResponse{
			ID:    product.ID,
			Name:  product.Name,
			Stock: product.Stock,
		})
	}

	return
}
