package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

var clear map[string]func() //create a map for storing clear funcs

func init() {
	clear = make(map[string]func()) //Initialize it
	clear["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func main() {
	treasureHunt := [6][8]bool{
		{false, false, false, false, false, false, false, false},
		{false, true, true, true, true, true, true, false},
		{false, true, false, false, false, true, true, false},
		{false, true, true, true, false, true, false, false},
		{false, true, false, true, true, true, true, false},
		{false, false, false, false, false, false, false, false},
	}

	ProbableTreasureHunt := [6][8]bool{
		{false, false, false, false, false, false, false, false},
		{false, false, true, false, false, true, false, false},
		{false, false, false, false, false, false, true, false},
		{false, false, false, false, false, false, false, false},
		{false, false, false, true, false, false, true, false},
		{false, false, false, false, false, false, false, false},
	}

	isWrongPosition := false
	isFounded := false

	startPositionX := 4
	startPositionY := 1

	tempStartPositionX := 4
	tempStartPositionY := 1

	treasureHuntX := 4
	treasureHuntY := 3

	reader := bufio.NewReader(os.Stdin)
	for {
		isWrongPosition = false
		for idx := range treasureHunt {
			if idx == 0 {
				fmt.Println("Press W for moving up")
				fmt.Println("Press D for moving right")
				fmt.Println("Press A for moving left")
				fmt.Println("Press S for moving down")
				fmt.Println("===================")
			}

			for idx2 := range treasureHunt[idx] {
				if startPositionX == treasureHuntX && startPositionY == treasureHuntY {
					isFounded = true
					break
				} else if idx == startPositionX && idx2 == startPositionY {
					if !treasureHunt[idx][idx2] {
						startPositionX = tempStartPositionX
						startPositionY = tempStartPositionY
						isWrongPosition = true
						break
					}
					if ProbableTreasureHunt[idx][idx2] {
						ProbableTreasureHunt[idx][idx2] = false
					}
					tempStartPositionX = startPositionX
					tempStartPositionY = startPositionY
					fmt.Print(" X")
					continue
				} else if treasureHunt[idx][idx2] && ProbableTreasureHunt[idx][idx2] {
					fmt.Print(" $")
					continue
				} else if !treasureHunt[idx][idx2] {
					fmt.Print(" #")
					continue
				}
				fmt.Print(" .")
			}
			fmt.Println("")
		}

		if isWrongPosition {
			CallClear()
			continue
		}

		if isFounded {
			CallClear()
			fmt.Println("Treasure Hunt Found")
			break
		}

		text, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			break
		}
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)

		switch text {
		case "w", "W":
			startPositionX -= 1
		case "d", "D":
			startPositionY += 1
		case "s", "S":
			startPositionX += 1
		case "a", "A":
			startPositionY -= 1
		}
		CallClear()
	}

}

func CallClear() {
	value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
	if ok {                          //if we defined a clear func for that platform:
		value() //we execute it
	} else { //unsupported platform
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}
