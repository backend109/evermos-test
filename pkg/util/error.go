package util

import (
	"fmt"

	"github.com/go-playground/validator/v10"
	"github.com/lib/pq"
	"gitlab.com/backend109/evermos-test/pkg/helper"
)

// * errorType used for choosing error types
func errorType(err error) (int, error) {
	switch {
	case isPqError(err):
		return helper.PqError(err)
	}
	return helper.CommonError(err)
}

// * isPqError used to check error if error is pg error
func isPqError(err error) bool {
	if _, ok := err.(*pq.Error); ok {
		return true
	}
	return false
}

// * switchErrorValidation used to check error on request validation
func switchErrorValidation(err error) (message string) {
	if castedObject, ok := err.(validator.ValidationErrors); ok {
		for idx, err := range castedObject {
			field := ToSnakeCase(err.Field())

			// Change Field Name
			switch field {
			}

			switch err.Tag() {
			case "required":
				message = fmt.Sprintf("%s is mandatory",
					field)
			default:
				message = err.Error()
			}

			if idx == 0 {
				break
			}
		}
	}
	return
}
