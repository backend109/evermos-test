package helper

import (
	"fmt"
	"net/http"
	"regexp"

	"github.com/lib/pq"
	"gitlab.com/backend109/evermos-test/constant"
)

var pqErrorMap = map[string]int{
	"unique_violation": http.StatusConflict,
}

// PqError is
func PqError(err error) (int, error) {
	re := regexp.MustCompile("\\((.*?)\\)")
	if err, ok := err.(*pq.Error); ok {
		match := re.FindStringSubmatch(err.Detail)
		// Change Field Name
		switch match[1] {
		}

		switch err.Code.Name() {
		case "unique_violation":
			return pqErrorMap["unique_violation"], fmt.Errorf("%s already exists", match[1])
		}
	}

	return http.StatusInternalServerError, fmt.Errorf(err.Error())
}

var commonErrorMap = map[error]int{
	constant.ErrorPgProductNotFound: http.StatusNotFound,
	constant.ErrorCommonOutOfStock:  http.StatusBadRequest,
}

// CommonError is
func CommonError(err error) (int, error) {
	switch err {
	case constant.ErrorPgProductNotFound:
		return commonErrorMap[constant.ErrorPgProductNotFound], constant.ErrorPgProductNotFound
	case constant.ErrorCommonOutOfStock:
		return commonErrorMap[constant.ErrorCommonOutOfStock], constant.ErrorCommonOutOfStock
	}
	return http.StatusInternalServerError, fmt.Errorf(err.Error())
}
