package constant

import "fmt"

type ErrorCommon error

var (
	ErrorCommonOutOfStock ErrorCommon = fmt.Errorf("out of stock")
)
