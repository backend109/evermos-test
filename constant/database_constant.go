package constant

import "fmt"

type ErrorPg error

var (
	ErrorPgProductNotFound ErrorPg = fmt.Errorf("product not found")
)
