package request

// * CreateOrderRequest used to create a new order
type CreateOrderRequest struct {
	ProductID int64 `json:"product_id" validate:"required" example:"1"`
	Quantity  int32 `json:"quantity" validate:"required" example:"1"`
}
