package response

type GetProductResponse struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	Stock int32  `json:"stock"`
}
