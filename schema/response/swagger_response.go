package response

// SwaggerGetListProduct for
type SwaggerGetListProduct struct {
	Base
	Data []GetProductResponse `json:"data"`
}

// SwaggerCreateOrder for
type SwaggerCreateOrder struct {
	Base
	Data interface{} `json:"data"`
}
